enum CompanyClassification {
  Bronze = 'Bronze',
  Silver = 'Silver',
  Gold = 'Gold',
}

/**
 * Base props of Company DB entity
 */
export default class CompanyDto {
  id: string
  name: string
  classification: CompanyClassification
}
