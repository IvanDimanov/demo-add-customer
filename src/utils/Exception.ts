import { v4 as uuidV4 } from 'uuid'

/**
 * This custom error throws a JSON object that can be easily extended and tracked
 */
export default class Exception extends Error {
  /**
   *
   * @param {string} code? This prop is used from other services that handle the error.
   *                       Useful when we translate the error message.
   * @param {string} message Human readable reason for the error that was created
   */
  constructor(code = 'EXCEPTION', message = 'Exception occurred') {
    /* Create a custom message as extendable JSON */
    super(JSON.stringify({
      id: uuidV4(), // Handy when looking at 1k logs output
      code,
      message,
    }, undefined, 2))
  }
}
