import Exception from '@src/utils/Exception'


describe('utils/Exception', () => {
  it('should extend Error', () => {
    expect(new Exception()).toBeInstanceOf(Error)
  })


  it('should have a JSON stringified message', () => {
    const exception = new Exception()
    expect(() => {
      JSON.parse(exception.message)
    }).not.toThrow()
  })


  it('should have a JSON stringified message with prop `id`', () => {
    const exception = new Exception()
    const message = JSON.parse(exception.message)
    expect(typeof message.id).toEqual('string')
  })


  it('should have a JSON stringified message with prop `code`', () => {
    const testCode = 'TEST_ERROR_CODE'
    const exception = new Exception(testCode)
    const message = JSON.parse(exception.message)

    expect(message.code).toEqual(testCode)
  })


  it('should have a JSON stringified message with prop `message`', () => {
    const testCode = 'TEST_ERROR_CODE'
    const testMessage = 'Test error message'
    const exception = new Exception(testCode, testMessage)
    const message = JSON.parse(exception.message)

    expect(message.message).toEqual(testMessage)
  })
})
