import Joi from 'joi'

const envVarSchema = {
  IMPORTANT_COMPANY_NAME: Joi
    .string()
    .min(1)
    .max(20)
    .default('VeryImportantClient'),

  DEFAULT_CREDIT_LIMIT: Joi
    .number()
    .integer()
    .min(1)
    .max(1000)
    .default(10),

  INVALID_CUSTOMER_FIRST_NAME: Joi
    .string()
    .min(1)
    .max(20)
    .default('Throw Exception'),
}


const { value, error } = Joi.object()
  .keys(envVarSchema)
  .required()
  .validate(process.env, {
    abortEarly: false,
    allowUnknown: true,
  })

/**
 * Make sure that:
 * - we're not leaking any unverified ENV VARs
 * - ENV VARs use default values
 * - ENV VARs have correct type
 */
process.env = value


if (error) {
  throw new ReferenceError(`Invalid ENV VAR:
${error.details
    .map(({ message, context }) => `  ${message}; currently ${context?.key}=${context?.value}`)
    .join('\n')}
\n`)
}
