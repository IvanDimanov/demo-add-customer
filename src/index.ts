import yargs from 'yargs'
import { validateOrReject } from 'class-validator'

import '@src/utils/validateEnvVars'

import { AddCustomerDto } from '@src/Customer/CustomerDto'
import CustomerService from '@src/Customer/CustomerService'


const argv = yargs(process.argv.slice(2))
  .usage('Usage: npm start -- [options]')
  .example(
    `npm start --
     --firstName John
     --lastName Smith
     --email john@smith.com
     --dateOfBirth 2000-01-01
     --companyId eccbbf85-06db-4cf4-96ee-905a43558f05`,
    'Create new Customer',
  )

  .describe('firstName', 'Customer first name')
  .alias('firstName', 'f')

  .describe('lastName', 'Customer last name')
  .alias('lastName', 'l')

  .describe('email', 'Customer email')
  .alias('email', 'e')

  .describe('dateOfBirth', 'Customer date of birth')
  .alias('dateOfBirth', 'd')

  .describe('companyId', 'Company to which the Customer is related to')
  .alias('companyId', 'c')

  .help('h')
  .alias('h', 'help')

  .string([
    'firstName',
    'lastName',
    'email',
    'dateOfBirth',
    'companyId',
  ])

  .demandOption([
    'firstName',
    'lastName',
    'email',
    'dateOfBirth',
    'companyId',
  ])
  .argv


;(async () => {
  const addCustomerData = new AddCustomerDto()
  addCustomerData.firstName = argv.firstName
  addCustomerData.lastName = argv.lastName
  addCustomerData.email = argv.email
  addCustomerData.dateOfBirth = new Date(argv.dateOfBirth)
  addCustomerData.companyId = argv.companyId

  try {
    await validateOrReject(addCustomerData)
  } catch (error) {
    throw new Error(error.map(({ constraints }) => Object.values(constraints).join(', ')).join('\n\t\t'))
  }

  return CustomerService.addCustomer(addCustomerData)
})()

.then((customer) => {
  process.stdout.write('\nCustomer was successfully added\n')
  process.stdout.write('-------------------------------\n')
  process.stdout.write(JSON.stringify(customer, undefined, 2))
  process.stdout.write('\n')
})

.catch((error) => {
  process.stderr.write(`\nError in adding new Customer: \n\t${error}\n\n`)
  process.exit(1)
})
