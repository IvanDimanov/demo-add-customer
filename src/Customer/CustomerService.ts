import CompanyModel from '@src/models/CompanyModel'
import CustomerModel from '@src/models/CustomerModel'

import CustomerDto, { AddCustomerDto, InsertCustomerDto } from '@src/Customer/CustomerDto'

const { IMPORTANT_COMPANY_NAME, DEFAULT_CREDIT_LIMIT } = process.env

/**
 * Common utility functions related to Customer business logic
 */
export default class CustomerService {
  /**
   * Create a new Customer entity in our system
   * @param {AddCustomerDto} addCustomerData Data we want to use when creating new Customer
   * @return {Promise<CustomerDto>} Saved Customer DB entiry
   */
  static async addCustomer(addCustomerData: AddCustomerDto): Promise<CustomerDto> {
    const { companyId, ...insertCustomerData } = addCustomerData
    const company = await CompanyModel.getById(companyId)

    const insertCustomer: InsertCustomerDto = {
      ...insertCustomerData,
      hasCreditLimit: true,
      creditLimit: Number.parseInt(DEFAULT_CREDIT_LIMIT as string),
      company: company,
    }

    /* Customers from "important company" do not have a credit limit */
    if (company.name === IMPORTANT_COMPANY_NAME) {
      insertCustomer.hasCreditLimit = false
      insertCustomer.creditLimit = undefined
    }

    const savedCustomer = await CustomerModel.insertCustomer(insertCustomer)
    /**
     * A good practice will be to send a welcome email to Customer
     * or notify Admin that a Customer from an "important company" was just added
     */

    return savedCustomer
  }
}
