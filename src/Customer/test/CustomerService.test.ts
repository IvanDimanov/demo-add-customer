import companies from '@src/demoDB/companies.json'

/* We need to set the `process.env` before it's been used from any of the modules below */
const IMPORTANT_COMPANY_NAME = companies[1].name
process.env.IMPORTANT_COMPANY_NAME = IMPORTANT_COMPANY_NAME

const DEFAULT_CREDIT_LIMIT = 25
process.env.DEFAULT_CREDIT_LIMIT = String(DEFAULT_CREDIT_LIMIT)


import CustomerService from '@src/Customer/CustomerService'
import CustomerDto, { AddCustomerDto } from '@src/Customer/CustomerDto'
import CompanyDto from '@src/Company/CompanyDto'


const customerTemplate: AddCustomerDto = {
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@smith.com',
  dateOfBirth: new Date(),
  companyId: companies[0].id,
}


describe('Customer/CustomerService', () => {
  describe('Customer/CustomerService#addCustomer()', () => {
    it('should be a function', () => {
      expect(CustomerService.addCustomer).toBeInstanceOf(Function)
    })


    it('should add new Customer', async () => {
      const { companyId, ...insertCustomerData } = customerTemplate
      const savedCustomer: CustomerDto = {
        ...insertCustomerData,
        id: companyId,
        hasCreditLimit: true,
        creditLimit: DEFAULT_CREDIT_LIMIT,
        company: companies[0] as CompanyDto,
      }
      const { id: savedId, ...expectedSavedCustomer } = savedCustomer

      const { id, ...customer } = await CustomerService.addCustomer(customerTemplate)

      expect(typeof id).toEqual(typeof savedId)
      expect(customer).toEqual(expectedSavedCustomer)
    })


    it('should add new Customer with lower case `email`', async () => {
      const { companyId, ...insertCustomerData } = customerTemplate
      const savedCustomer: CustomerDto = {
        ...insertCustomerData,
        id: companyId,
        hasCreditLimit: true,
        creditLimit: DEFAULT_CREDIT_LIMIT,
        company: companies[0] as CompanyDto,
      }
      const { id: savedId, ...expectedSavedCustomer } = savedCustomer

      const { id, ...customer } = await CustomerService.addCustomer({
        ...customerTemplate,
        email: insertCustomerData.email.toUpperCase(),
      })

      expect(typeof id).toEqual(typeof savedId)
      expect(customer).toEqual(expectedSavedCustomer)
    })


    it('should add new Customer with unlimited credit when Customer is related to "important company"', async () => {
      const { companyId, ...insertCustomerData } = customerTemplate
      const savedCustomer: CustomerDto = {
        ...insertCustomerData,
        id: companyId,
        hasCreditLimit: false,
        creditLimit: undefined,
        company: companies[1] as CompanyDto,
      }
      const { id: savedId, ...expectedSavedCustomer } = savedCustomer

      const { id, ...customer } = await CustomerService.addCustomer({
        ...customerTemplate,
        companyId: companies[1].id,
      })

      expect(typeof id).toEqual(typeof savedId)
      expect(customer).toEqual(expectedSavedCustomer)
    })
  })
})
