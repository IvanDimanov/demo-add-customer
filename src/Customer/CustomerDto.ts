/* eslint-disable new-cap */
import {
  Length,
  IsEmail,
  IsDate,
  MinDate,
  MaxDate,
  IsUUID,
} from 'class-validator'
import { subYears } from 'date-fns'

import CompanyDto from '@src/Company/CompanyDto'

/**
 * This DTO is used to validate user input.
 * If user input is valid, we'll use it for initializing the "add customer" process
 */
export class AddCustomerDto {
  @Length(2, 100)
  firstName: string

  @Length(2, 100)
  lastName: string

  @IsEmail()
  email: string

  @IsDate()
  @MinDate(subYears(new Date(), 100), { message: 'Customer must be younger than 100 years' })
  @MaxDate(subYears(new Date(), 21), { message: 'Customer must be older than 21 years' })
  dateOfBirth: Date

  @IsUUID('4')
  companyId: string
}

/**
 * This DTO represents the data needed to create/insert new Customer in the DB
 */
export class InsertCustomerDto {
  firstName: string
  lastName: string
  dateOfBirth: Date
  email: string
  hasCreditLimit: boolean
  creditLimit?: number
  company: CompanyDto
}

/**
 * Base props of Customer DB entity
 */
 export default class CustomerDto extends InsertCustomerDto {
  id: string
}
