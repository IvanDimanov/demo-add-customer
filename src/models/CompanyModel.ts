﻿import CompanyDto from '@src/Company/CompanyDto'

import Exception from '@src/utils/Exception'
import delay from '@src/utils/delay'

import companies from '@src/demoDB/companies.json'

/**
 * DB accessor to Company entity model
 */
export default class CompanyModel {
  /**
   * This function finds a Company in our DB based on a `id` prop
   * @param {string} id Company ID used to find a company
   * @return {Promise<CompanyDto>} Found company DB entity
   */
  static async getById(id: string): Promise<CompanyDto> {
    /* Simulate DB access */
    await delay(100)

    const company = companies.find((company) => company.id === id)
    if (!company) {
      throw new Exception('COMPANY_NOT_FOUND', `Company with "id" = "${id}" was not found in the DB`)
    }

    return company as CompanyDto
  }
}
