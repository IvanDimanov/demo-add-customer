﻿import { v4 as uuidV4 } from 'uuid'

import CustomerDto, { InsertCustomerDto } from '@src/Customer/CustomerDto'

import Exception from '@src/utils/Exception'
import delay from '@src/utils/delay'

const { INVALID_CUSTOMER_FIRST_NAME } = process.env

/**
 * DB accessor to Customer entity model
 */
export default class CustomerModel {
  /**
   * Be able to save new Customers in the DB
   * @param {InsertCustomerDto} customer Props we want to save in the DB as new Customer entity
   * @return {Promise<CustomerDto>} Newly saved Customer DB entity
   */
  static async insertCustomer(customer: InsertCustomerDto): Promise<CustomerDto> {
    /* It's a good practice to secure emails saved in lower case */
    customer.email = customer.email.toLocaleLowerCase()

    /* Simulate DB access */
    await delay(100)

    /* Simulate DB issue when inserting a record */
    if (customer.firstName === INVALID_CUSTOMER_FIRST_NAME) {
      throw new Exception('CUSTOMER_NOT_SAVED', 'There`s a DB issue saving customer at the moment. Please try again later')
    }

    return {
      id: uuidV4(),
      ...customer,
    }
  }
}
