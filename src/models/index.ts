/**
 * Normally, here we declare a base class from which all DB Models inherit common functions such as:
 *   - find
 *   - findOne
 *   - findById
 *   - delete
 *   - softDelete
 *
 * Example of `BaseModel`:
 *   https://github.com/IvanDimanov/demo-ecommerce-backend/blob/master/src/database/models/Base.ts
 */
