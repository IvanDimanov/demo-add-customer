/* We need to set the `process.env` before it's been used from any of the modules below */
const INVALID_CUSTOMER_FIRST_NAME = 'Test invalid customer first name'
process.env.INVALID_CUSTOMER_FIRST_NAME = INVALID_CUSTOMER_FIRST_NAME


import CustomerModel from '@src/models/CustomerModel'
import { InsertCustomerDto } from '@src/Customer/CustomerDto'
import CompanyDto from '@src/Company/CompanyDto'

import companies from '@src/demoDB/companies.json'


const customerTemplate: InsertCustomerDto = {
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@smith.com',
  dateOfBirth: new Date(),
  hasCreditLimit: true,
  creditLimit: 21,
  company: companies[0] as CompanyDto,
}


describe('models/CustomerModel', () => {
  describe('models/CustomerModel#insertCustomer()', () => {
    it('should be a function', () => {
      expect(CustomerModel.insertCustomer).toBeInstanceOf(Function)
    })


    it('should insert new Customer', async () => {
      const { id, ...customer } = await CustomerModel.insertCustomer(customerTemplate)

      expect(typeof id).toEqual('string')
      expect(customer).toEqual(customerTemplate)
    })


    it('should insert new Customer with lower case `email`', async () => {
      const { id, ...customer } = await CustomerModel.insertCustomer({
        ...customerTemplate,
        email: customerTemplate.email.toLocaleUpperCase(),
      })

      expect(typeof id).toEqual('string')
      expect(customer).toEqual(customerTemplate)
    })


    it('should throw when called with invalid Customer `firstName`', async () => {
      await expect(
        CustomerModel.insertCustomer({
          ...customerTemplate,
          firstName: INVALID_CUSTOMER_FIRST_NAME,
        })
      )
      .rejects
      .toThrow()
    })
  })
})
