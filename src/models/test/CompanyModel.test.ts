import CompanyModel from '@src/models/CompanyModel'
import companies from '@src/demoDB/companies.json'


describe('models/CompanyModel', () => {
  describe('models/CompanyModel#getById()', () => {
    it('should be a function', () => {
      expect(CompanyModel.getById).toBeInstanceOf(Function)
    })

    it('should return a Company when called with existing `id`', async () => {
      const company = await CompanyModel.getById(companies[0].id)
      expect(company).toEqual(companies[0])
    })

    it('should throw when called with nonexisting `id`', async () => {
      await expect(
        CompanyModel.getById('nonexisting id')
      )
      .rejects
      .toThrow()
    })
  })
})
