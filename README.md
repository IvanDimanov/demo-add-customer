# Demo add Customer
Demo app to add Customer using a CLI tool.


[![Install video](https://gitlab.com/IvanDimanov/demo-add-customer/-/raw/master/index.gif)](https://asciinema.org/a/416532)


## Running locally
Install video: https://asciinema.org/a/416532
```
git clone git@gitlab.com:IvanDimanov/demo-add-customer.git
cd demo-add-customer
npm ci
npm run add-customer-success
```


## Tests
Running unit tests:
```
npm run test
```

Generate unit test coverage:
```
npm run test-coverage
```
Then you can open `./coverage/index.html`.

Running E2E tests:
```
npm run test-e2e
```


## Tech stack
- [Node.js](https://nodejs.org) + [TypeScript](https://www.typescriptlang.org) - scaffolding
- [class-validator](https://github.com/typestack/class-validator) - validate inputs against class
- [Joi](https://www.npmjs.com/package/joi) - schema validation used for ENV VARs
- [yargs](http://yargs.js.org) - CLI template tools
- [date-fns](https://date-fns.org) - Date handling


## File & Folder structure
Here we describe what you can find and where.
```
/src
  /Company
    -- Main module that holds Company related DTO

  /Customer
    -- Main module that holds Customer related DTO and Service functions

  /demoDB
    -- Useful JSON records used to simulate DB access

  /models
    -- DB accessors used to handle Company and Customer DB entities

  /utils
    -- Common set of handy functions that manage common delay, Exceptions, etc.

/e2e
  -- Holds all End-to-End tests
```
