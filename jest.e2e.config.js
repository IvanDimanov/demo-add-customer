module.exports = {
  rootDir: './e2e',
  preset: 'ts-jest',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
}
