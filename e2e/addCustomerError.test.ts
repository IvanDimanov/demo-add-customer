import shell from 'shelljs'
import { format } from 'date-fns'

import companies from '../src/demoDB/companies.json'


type GenerateCommandProps = {
  firstName: string,
  lastName: string,
  email: string,
  dateOfBirth: Date,
  companyId: string,
}

const defaultProps = {
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@smith.com',
  dateOfBirth: new Date('2000-01-01'),
  companyId: companies[0].id,
}

const generateCommand = (props: GenerateCommandProps = defaultProps): string => 'npm start -- ' +
  `--firstName ${props.firstName} ` +
  `--lastName ${props.lastName} ` +
  `--email ${props.email} ` +
  `--dateOfBirth ${format(props.dateOfBirth, 'yyyy-MM-dd')} ` +
  `--companyId ${props.companyId}`


describe('Check Customer errors', () => {
  it('should exit with status code of 1 when `firstName` is not sent', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        firstName: '',
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `firstName` is invalid', () => {
    const firstName = 'Invalid-first-name'
    shell.env['INVALID_CUSTOMER_FIRST_NAME'] = firstName
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        firstName,
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `lastName` is not sent', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        lastName: '',
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `email` is not sent', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        email: '',
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `email` is invalid', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        email: 'invalid email',
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `dateOfBirth` is less than 21 years ago', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        dateOfBirth: new Date('2020-01-01'),
      }), { silent: true }).code,
    ).toBe(1)
  })


  it('should exit with status code of 1 when `companyId` is not found in the DB', () => {
    expect(
      shell.exec(generateCommand({
        ...defaultProps,
        companyId: 'invalid-company-id',
      }), { silent: true }).code,
    ).toBe(1)
  })
})
