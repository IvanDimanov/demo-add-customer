import shell from 'shelljs'
import JSON5 from 'json5'
import { format } from 'date-fns'

import companies from '../src/demoDB/companies.json'


type GenerateCommandProps = {
  firstName: string,
  lastName: string,
  email: string,
  dateOfBirth: Date,
  companyId: string,
}

const defaultProps = {
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@smith.com',
  dateOfBirth: new Date('2000-01-01'),
  companyId: companies[0].id,
}

const generateCommand = (props: GenerateCommandProps = defaultProps): string => 'npm start -- ' +
  `--firstName ${props.firstName} ` +
  `--lastName ${props.lastName} ` +
  `--email ${props.email} ` +
  `--dateOfBirth ${format(props.dateOfBirth, 'yyyy-MM-dd')} ` +
  `--companyId ${props.companyId}`


describe('Add new Customer', () => {
  it('should exit with status code of 0', () => {
    expect(
      shell.exec(generateCommand(), { silent: true }).code,
    ).toBe(0)
  })


  it('should create new Customer with regular credit limit', () => {
    const creditLimit = 21
    shell.env['DEFAULT_CREDIT_LIMIT'] = creditLimit

    const { stdout } = shell.exec(generateCommand(), { silent: true })

    const startOfJson = stdout.indexOf('{')
    const endOfJson = stdout.lastIndexOf('}')
    const customerStringified = stdout.substr(startOfJson, endOfJson)

    const { id, ...customerData } = JSON5.parse(customerStringified)
    const { companyId, ...expectedCustomerData } = defaultProps

    expect(typeof id).toEqual(typeof companyId)
    expect(customerData).toEqual({
      ...expectedCustomerData,
      hasCreditLimit: true,
      creditLimit,
      dateOfBirth: expectedCustomerData.dateOfBirth.toISOString(),
      company: companies[0],
    })
  })


  it('should create new Customer with lower case `email`', () => {
    const creditLimit = 21
    shell.env['DEFAULT_CREDIT_LIMIT'] = creditLimit

    const { stdout } = shell.exec(generateCommand({
      ...defaultProps,
      email: defaultProps.email.toLocaleUpperCase(),
    }), { silent: true })

    const startOfJson = stdout.indexOf('{')
    const endOfJson = stdout.lastIndexOf('}')
    const customerStringified = stdout.substr(startOfJson, endOfJson)

    const { id, ...customerData } = JSON5.parse(customerStringified)
    const { companyId, ...expectedCustomerData } = defaultProps

    expect(typeof id).toEqual(typeof companyId)
    expect(customerData).toEqual({
      ...expectedCustomerData,
      hasCreditLimit: true,
      creditLimit,
      dateOfBirth: expectedCustomerData.dateOfBirth.toISOString(),
      company: companies[0],
    })
  })


  it('should create new Customer with unlimited credit limit', () => {
    shell.env['IMPORTANT_COMPANY_NAME'] = companies[0].name

    const { stdout } = shell.exec(generateCommand(), { silent: true })

    const startOfJson = stdout.indexOf('{')
    const endOfJson = stdout.lastIndexOf('}')
    const customerStringified = stdout.substr(startOfJson, endOfJson)

    const { id, ...customerData } = JSON5.parse(customerStringified)
    const { companyId, ...expectedCustomerData } = defaultProps

    expect(typeof id).toEqual(typeof companyId)
    expect(customerData).toEqual({
      ...expectedCustomerData,
      hasCreditLimit: false,
      dateOfBirth: expectedCustomerData.dateOfBirth.toISOString(),
      company: companies[0],
    })
  })
})
