const fs = require('fs')
const path = require('path')
const JSON5 = require('json5')

const tsconfigStringified = fs.readFileSync(path.resolve(__dirname, './tsconfig.json'))
const { compilerOptions } = JSON5.parse(tsconfigStringified)

const moduleNameMapper = Object.keys(compilerOptions.paths)
  .map((key) => {
    const alias = key.replace('*', '(.*)').replace('@', '^@')
    const path = `<rootDir>/${compilerOptions.paths[key][0].replace('*', '$1')}`
      .replace('@', '^@')

    /**
     * Target key <-> value relation:
     *   '^@src/(.*)': '<rootDir>/src/$1',
     */
    return {
      [alias]: path,
    }
  })
  .reduce((aliases, alias) => ({ ...aliases, ...alias }), {})


module.exports = {
  rootDir: './src',
  preset: 'ts-jest',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  moduleNameMapper,
  coverageDirectory: '../coverage',
  coverageReporters: ['text', 'html'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10,
    },
  },
}
